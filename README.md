# Navodila za integracijo Cubilis sistema na spletno stran

> Avtorji zapisanega: Forward, Google Partner

Kratka navodila z napotki za integracijo Cubilis sistema na spletno stran.

> Nasvet: Primer dobre prakse končnega vmesnika in povezave `spletna stran <-> Cubilis` si lahko pogledate na [https://www.terme-topolsica.si/](https://www.terme-topolsica.si/)

## Integracija Ratebox widgeta
Koda, zadolžena za prikaz Raatebox Cubilis Widgeta, se vstavi v oziroma za `<footer>` HTML element, preden se zaključi `</body>`. Kodo vam posreduje stranka, najde jo v Cubilis sistemu. Primer kode:
```html
<script async defer type="text/javascript" src="https://static.cubilis.eu/js/ratebox.bundle.js"></script>
<script async defer type="text/javascript">
  new Ratebox({
    IBEUrl: '<name_of_the_cubilis_client>',
    key: '<client_key>',
    locale: 'en-GB' // or other valid locale, for example sl-SI or de-DE...
  });
</script>
```
Za optimalen page speed perfomance je pomembno, da je tako generalni Cubilis Ratebox skripti kot custom `<script>` znački dodan atribut `async` in `defer`.

*Skripta se lahko pokliče tudi v glavni (naprimer) main.js datoteki, pri čemer je treba paziti, da se glavna cubilis skripta naloži preden se pokliče Ratebox constructor, v nasprotnem primeru bo konzola vrnila napako `Uncaught ReferenceError: Ratebox is not defined`. Predstavljen je zgolj naenostavnejši način integracije, ki je med drugim mogoča tudi prek Google Tag Managerja.*

## Integracija obrazca za rezervacijo
Cubilis OOTB ne ponuja "widgeta" z obrazcem za rezervacijo, tako da je potrebno ta del implementirati ročno. Integracija je sicer možna na več različnih načinov, predstavili bomo način, ki se ga poslužujemo sami.

Kako se URL naslovu dodajo parametri, ki bodo opisani v nadaljevanju, je načeloma stvar razvijalca in nima vpliva na končni rezultat. Naš predlog je, da se parametri pripenjajo dinamično s pomočjo JavaScripta, ni pa nujno.

### Splošni URL naslov za rezervacijo
Končni (splošni) URL naslov za rezervacijo izgleda tako: `https://reservations.cubilis.eu/<client-name>/?Language=sl-SI`. Tak URL naslov uporabnika pelje iz spletne strani klienta na naslovno stran Cubilis sistema s podanim Language parametrom (ki naj se ujema s trenutno izbranim jezikom uporabnika na spletni strani klienta).

### Dodajanje parametra za datum prihoda in odhoda
Velikokrat želimo uporabniku ponuditi možnonst izbire datuma prihoda in odhoda že na sami spletni strani s ciljem čim lažjega in hitrejšega postopka rezervacije v sistemu Cubilis. To lahko naredimo tako, da splošnemu URL naslovu dodamo najprej `path` -> `/Rooms/Select` in nato pripnemo parametra `Arrival` in `Departure` (ter ohranimo parameter Language). Format datuma za parametra `Arrival` in `Departure` mora biti v obliki **YYYY-MM-DD**. Vrstni red parametrov sicer ni pomemben, zaradi preglednosti in smiselnosti kode pa je naš predlog, da je vrstni red parametrov `Arrival`, `Departure`, `Language`.  Končni URL naslov bi izgledal nekako tako:
```
https://reservations.cubilis.eu/<client-name>/Rooms/Select?Arrival=2020-03-05&Departure=2020-03-07&Language=sl-SI
```

### Dodajanje parametra za izbor posamezne sobe (namestitve)
URL naslovu lahko pripnemo tudi parameter za izbiro sobe. Parameter lahko pripnemo ali v kombinaciji s parametroma datuma prihoda in odhoda, ali pa kot samostojen parameter. `Path` je v vsakem primeru enak, kot pri podajanju datuma prihoda in odhoda (torej splošnemu URL naslovu je dodano `/Rooms/Select`). Parameter za izbiro sobe je **`Room`** z vrednostjo RoomID, ki jo najdemo tako, da pregledamo izvorno kodo v Cubilis sistemu (desni klik na posamezno sobo -> inspect element -> `div` element ima pripet atribut `data-room` z vrednostjo, ki jo iščemo). Primer končnega URL naslova (v spodnjem primeru brez datumov prihoda in odhoda, lahko pa bi bili dodani):
```
https://reservations.cubilis.eu/<client-name>/Rooms/Select?Room=<room_id>&Language=sl-SI
```

> Predlog: Za izbor/vnos datumov v obrazec za rezervacijo na spletni strani, sami, s ciljem po boljši uporabniški izkušnji izbora teh datumov, uporabljamo JS knjižnico Flatpickr.js ([https://flatpickr.js.org/examples/](https://flatpickr.js.org/examples/)). Ni pogoj, vnosno polje za izbiro datuma je načeloma lahko tudi klasičen text input field (don't do that), je pa primer dobre prakse.


### Dodajanje parametra za izbor posameznega paketa
Se implementira na povsem enak način, kot je opisano za dodajanje parametra za izbor sobe, le da ne dodamo parametra Room temveč **`Package`** in v Cubilis sistemu v izvorni kodi poiščemo atribut `data-package`.
Primer končnega URL naslova:
```
https://reservations.cubilis.eu/<client-name>/Rooms/Select?Package=<package_id>&Language=sl-SI
```


#### Primer pripenjanja parametrov s pomočjo JavaScripta
Sami se največkrat poslužujemo pripenjanja parametrov s pomočjo JS, saj je koda tako najbolj dinamično zastavljena in fleksibilna za različne primere uporabe na spletni strani. Za validacijo in določanje datumov si pomagamo s knjižnico Day.js ([https://github.com/iamkun/dayjs](https://github.com/iamkun/dayjs)).

**Primer dinamičnega pripenjaja jezika vmesnika in datumov za gumb posamezne nastanitve (sobe):**
```javascript
const handleRoomBtnClick = (el, e, dayDifference) => {
  e.preventDefault();
  const targetUrl = new URL(el.href);
  const targetUrlSearchParams = new URLSearchParams(targetUrl.search);

  const daysToAdd = dayDifference ? dayDifference : 1;

  const siteLanguage = document.documentElement.lang || "en-GB";

  const arrivalDate = dayjs().format("YYYY-MM-DD");
  const departureDate = dayjs()
    .add(daysToAdd, "day")
    .format("YYYY-MM-DD");

  targetUrlSearchParams.append("Arrival", arrivalDate);
  targetUrlSearchParams.append("Departure", departureDate);
  targetUrlSearchParams.append("Language", siteLanguage);

  targetUrl.search = targetUrlSearchParams;
  window.open(targetUrl, "_blank");
};

// Query for all buttons on website with cubilis-btn class and call handleRoomBtnClick fn
document.querySelectorAll('a.cubilis-btn', btn => {
	btn.addEventListener('click', e => handleRoomBtnClick(btn, e))
});
```
Ta primer enostavno lahko prenesemo tudi na obrazec, kjer lahko uporabnik izbere datum prihoda in odhoda:
```javascript
const handleReservationFormSubmit = (form, e) => {
  e.preventDefault();
  const baseUrl = 'https://reservations.cubilis.eu/<client-name>/Rooms/Select';
  const targetUrl = new URL(baseUrl);
  const targetUrlSearchParams = new URLSearchParams(targetUrl.search);

  const siteLanguage = document.documentElement.lang || "en-GB";

  const arrivalDate = form.querySelector('input[name="Arrival"]').value;
  const departureDate = form.querySelector('input[name="Departure"]').value;

  targetUrlSearchParams.append("Arrival", arrivalDate);
  targetUrlSearchParams.append("Departure", departureDate);
  targetUrlSearchParams.append("Language", siteLanguage);

  targetUrl.search = targetUrlSearchParams;
  window.open(targetUrl, "_blank");
};

// Query for all buttons on website with cubilis-btn class and call handleRoomBtnClick fn
document.querySelectorAll('form.cubilis-form', form => {
	form.addEventListener('submit', e => handleReservationFormSubmit(form, e))
});
```

Kot rečeno je načinov implementacije nešteto, implementacija se lahko prilagodi tudi glede na potrebe stranke. Prikazan je primer osnovne integracije.

#### Primer pripenjanja parametrov s pomočjo GET metode `<form>` HTML elementa
Primer posredovanja podatkov iz obrazca brez JavaScript-a. Code snippet je vzet iz ene od naših implementacij in je zelo specifičen glede na razvojno okolje, kjer smo ga uporabili (PHP -> Wordpress -> SiteOrigin Page Builder + WPML vtičnik za več jezičnost).
```php
$datetime = new DateTime('NOW');
$tomorrow = new DateTime('tomorrow');

$search = $instance['search_url'];
$language = $instance['cubilis_language'] ?: 'en_GB';

$today   = current_time('timestamp', 1);
$tomorow = strtotime("+1 day", $today);

$format = get_option('date_format');

$check_in_date  = '';
$check_out_date = '';

if (is_plugin_active('sitepress-multilingual-cms/sitepress.php')) {
    if ($check_in_date == '') {
        $check_in_date = $datetime->format($format);
    }
    if ($check_out_date == '') {
        $check_out_date = $tomorrow->format($format);
    }
} else {
    if ($check_in_date == '') {
        $check_in_date = $datetime->format($format);
    }
    if ($check_out_date == '') {
        $check_out_date = $tomorrow->format($format);
    }
}

$uniqid = uniqid();

?>

<div id="fwd-reservation-<?php echo uniqid(); ?>" class="fwd-reservation">
	<form name="fwd-reservation-form" method="GET" target="_blank" action="<?php echo $search; ?>" class="fwd-reservation-form-<?php echo esc_attr($uniqid); ?>">
		<ul class="fwd-reservation-form-fields">
			<li class="fwd-reservation-form-field fwd-reservation-form-check-in">
				<div class="label"><?php echo esc_html__('Prihod', 'fwd'); ?></div>
				<div class="fwd-reservation-form-field-input">
					<input type="text" name="Arrival" id="arrival" class="day arrival" value="<?php echo $datetime->format('Y-m-d'); ?>" />
					<span class="toggle-date"><i class="ion-ios-arrow-down"></i></span>
				</div>
			</li>
			<li class="fwd-reservation-form fwd-reservation-form-check-out">
				<div class="label"><?php echo esc_html__('Odhod', 'fwd'); ?></div>
				<div class="fwd-reservation-form-field-input">
					<input type="text" name="Departure" id="departure" data-depends-on-arrival="true" class="day departure" value="<?php echo $tomorrow->format('Y-m-d'); ?>" />
					<span class="toggle-date"><i class="ion-ios-arrow-down"></i></span>
				</div>
			</li>
		</ul>
		<?php
			ob_start();
			ob_get_clean();
		?>
		<input type="hidden" name="Language" value="<?php echo $language; ?>"/>
		<p class="hb-submit">
			<button type="submit">
				<?php echo esc_html__('Preveri', 'fwd'); ?>
				<br>
				<?php echo esc_html__('razpoložljivost', 'fwd'); ?>
			</button>
		</p>
	</form>
</div>
```

#### Primer statično vstavljenega URL naslova
URL naslov lahko vedno podamo direktno kot `href` parameter anchor tag html elementa:
```html
<a href="https://reservations.cubilis.eu/<client-name>/Rooms/Select?Arrival=2020-03-05&Departure=2020-03-07&Language=sl-SI">Rezerviraj</a>
```
a smo na ta način zelo omejeni z "hard coded" vrednostmi. Primer direktnega vnosa bi bil recimo za splošen gumb "Rezeviraj", kjer uporabnika zgolj premaknemo iz spletne strani stranke na spletno stran Cubilis sistema.